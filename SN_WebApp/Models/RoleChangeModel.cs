﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SN_WebApp.Models
{
    public class RoleChangeModel
    {
        public string UserName { get; set; }
        public string NewRole { get; set; }
    }
}
