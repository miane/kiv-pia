﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SN_WebApp.Models
{
    public class UserToFriendModel
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string FriendId { get; set; }
        public string FriendName { get; set; }
        public bool Accepted { get; set; }

        public bool Blocked { get; set; }
        public bool FriendOnline { get; set; }

    }
}
