﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN_WebApp.Models
{
    public class MessageModel
    {

        public int Id { get; set; }
        public string FromUser { get; set; }

        public string ToUser { get; set; }
        public string Message { get; set; }

        public bool CurrentUser { get; set; }

        public DateTime DateSent { get; set; }

        public int Like { get; set; }
    }
}
