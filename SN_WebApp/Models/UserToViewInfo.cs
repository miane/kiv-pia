﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;

namespace SN_WebApp.Models
{
    public class UserToViewInfo
    {
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Role { get; set; }
        public string AvatarName { get; set; }
        public bool IsOnline { get; set; }

    }
}
