﻿using SN_WebApp.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SN_WebApp.Data
{
    public class NetworkDBContext : IdentityDbContext
    {

        public DbSet<FriendListEntity> friendList { get; set; }

        public DbSet<MyUserEntity> userInfoList { get; set; }

        public DbSet<Post> postList { get; set; }

        public DbSet<LikeForPost> likeList { get; set; }


       public DbSet<MessagesByUsers> messagesList { get; set; }

        public NetworkDBContext(DbContextOptions<NetworkDBContext> opt) : base(opt)
        {

        }
    }
}
