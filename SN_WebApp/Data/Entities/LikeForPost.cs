﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SN_WebApp.Data.Entities
{
    public class LikeForPost
    {
        public int Id { get; set; }

        public int PostID { get; set; }
        
        [ForeignKey(nameof(PostID))]

        public virtual Post Post { get; set; }
        public string UserID { get; set; }



    }
}
