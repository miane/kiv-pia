﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SN_WebApp.Data.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public string UserID { get; set; }

        [ForeignKey(nameof(UserID))]
        public virtual IdentityUser User { get; set; }

        public string UserName { get; set; }
        public string Content { get; set; }

        public DateTime InsertionDate  { get; set; }

        public int Like { get; set; }
    }
}
