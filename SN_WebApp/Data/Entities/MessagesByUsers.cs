﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace SN_WebApp.Data.Entities
{
    public class MessagesByUsers
    {
        public int Id { get; set; }
        public string UserID { get; set; }

        [ForeignKey(nameof(UserID))]
        public virtual IdentityUser User { get; set; }
        public string FriendID { get; set; }

        [ForeignKey(nameof(FriendID))]
        public virtual IdentityUser Friend { get; set; }

        public string Content { get; set; }

        public DateTime DateSent { get; set; }

    }
}
