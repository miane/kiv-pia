﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SN_WebApp.Data.Entities
{
    public class MyUserEntity
    {
        public int Id { get; set; }
        public string UserID { get; set; }

        [ForeignKey(nameof(UserID))]
        public virtual IdentityUser User { get; set; }
        public string Avatar { get; set; }
        public bool Active { get; set;}
    }
}
