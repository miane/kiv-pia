﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using SN_WebApp.Services;
using SN_WebApp.Models;
using System.Collections.Generic;
using SN_WebApp.Repositories;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace SN_WebApp.Hubs
{
    public class MessageHub : Hub
    {

        private ILogger<MessageHub> _logger;
        private IUserService _userService;
        private IPostService _postService;
        private IFriendService _friendService;  
        private UserManager<IdentityUser> _userManager;

        public static List<(string, string)> connectedUsers = new List<(string, string)>();
        public MessageHub(ILogger<MessageHub> logger, IUserService userService, IPostService postService, UserManager<IdentityUser> userManager, IFriendService friendService)
        {
            this._logger = logger;
            this._userService = userService;
            this._postService = postService;          
            this._userManager = userManager;
            this._friendService = friendService;
        }

        public async Task SendMessageToUser(string fromUser, string toUser, string message)
        {

            if (toUser == "connection" || toUser == "friend" || toUser == "notif" || message == "online")
            { 
                await Clients.All.SendAsync("ReceiveMessage", fromUser, toUser, message);
            }
            else 
            {
                await _friendService.SaveMessages(fromUser, toUser, message);
                await Clients.All.SendAsync("ReceiveMessage", fromUser, toUser, message);
            }
        }
        public async Task SendPost(string user, string message)
        {
            if (message.CompareTo("itslike") != 0)
            {
                _postService.SavePostAsync(user, message);
            }      
            await Clients.All.SendAsync("ReceivePost", user, message);

        }

        public async Task SendNotif(string user, string message)
        {
            _postService.SavePostAsync(user, message);
            await Clients.All.SendAsync("ReceiveNotif", user, message);

        }

        public override Task OnConnectedAsync()
        {
            //_logger.LogInformation("User notify: " + Context.User.Identity.IsAuthenticated);
            connectedUsers.Add((Context.ConnectionId, Context.User.Identity.Name));
            _userService.AddUser(_userService.GetUserByName(Context.User.Identity.Name).UserId, true); 
            return base.OnConnectedAsync();
            //Clients.All.SendAsync("UserUpdate", GetOnlineUsers());
        }

        public override async Task OnDisconnectedAsync(Exception e)
        {
            
            //Clients.All.SendAsync("UserUpdate", GetOnlineUsers());
            connectedUsers.Remove((Context.ConnectionId, Context.User.Identity.Name));
            _userService.AddUser(_userService.GetUserByName(Context.User.Identity.Name).UserId, false);
            await base.OnDisconnectedAsync(e);
        }

    }
}
