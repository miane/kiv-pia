﻿using SN_WebApp.Models;
using SN_WebApp.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SN_WebApp.Repositories
{
    public interface IUserRepository
    {
        int AddUserAvatar(string userId, string avatar);
        int AddUserInfo(string userId, bool isOnline);
        Task<IdentityResult> AddPhone(string userId, string phone);
        bool ChangeUserPassword(string userId, string newPass);
        bool ChangeUserPasswordValidate(string userId, string newPass, string oldPass);
        void ChangeUserRole(string userId, string newRole);
        List<UserToFriendModel> GetAllRealationships(string userId);
        List<UserToViewInfo> GetAllUsers();
        string GetIdByName(string userName);
        //Task DeleteUserAsync(string userId);

        public void RemoveUserRole(string userId, string newRole);
        Task<List<Post>> GetPost();
        Task SavePost(string userName, string message);
        Task<List<LikeForPost>> GetLikeUsers(int postId);
        Task AddLike(string userName, int postId);

        Task RemoveLike(string userName, int postId);
        UserToViewInfo GetUserById(string userId);
        UserToViewInfo GetUserByName(string userName);
        bool HasFriendRelation(string fromUserId, string toUserId);
        int ProcessFriendRequest(string fromUserId, string toUserId, bool accepted);
        int RemoveFriend(string fromUserId, string toUserId);
        int SendBlockingRequest(string fromUserId, string toUserId);
        int SendFriendRequest(string fromUserId, string toUserId);

        Task RemoveMessage(string fromUserId, string toUserId);

        Task SaveMessages(string fromUserId, string toUserId, string content);
        Task<List<MessagesByUsers>> GetOldMessages(string fromUserId, string toUserId);
    }
}