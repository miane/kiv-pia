﻿using SN_WebApp.Models;
using SN_WebApp.Data;
using SN_WebApp.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SN_WebApp.Repositories
{
    public class UserRepository : IUserRepository
    {
        private NetworkDBContext _dbContext;
        private UserManager<IdentityUser> _userMan;

        private RoleManager<IdentityRole> _roleMan;
        

        public UserRepository(NetworkDBContext context, UserManager<IdentityUser> userMan)
        {
            _dbContext = context;
            _userMan = userMan;
           
        }

        public bool ChangeUserPassword(string userId, string newPass)
        {
            var user = _userMan.FindByIdAsync(userId).Result;
            if (_userMan.RemovePasswordAsync(user).Result.Succeeded)
            {
                return _userMan.AddPasswordAsync(user, newPass).Result.Succeeded;
            }
            return false;
        }

        public bool ChangeUserPasswordValidate(string userId, string newPass, string oldPass)
        {
            var user = _userMan.FindByIdAsync(userId).Result;
            return _userMan.ChangePasswordAsync(user, oldPass, newPass).Result.Succeeded;
        }

        public string GetIdByName(string userName)
        { 
            return _dbContext.Set<IdentityUser>().Where(u => u.UserName == userName).Select(o => o.Id).FirstOrDefault();
        }
        public UserToViewInfo GetUserByName(string userName)
        {
            var userId = _dbContext.Set<IdentityUser>().Where(u => u.UserName == userName).Select(o => o.Id).FirstOrDefault();
            var roleId = _dbContext.Set<IdentityUserRole<string>>().Where(o => o.UserId == userId).Select(o => o.RoleId).FirstOrDefault();
            UserToViewInfo user = new UserToViewInfo()
            {
                UserId = userId,
                UserName = _dbContext.Set<IdentityUser>().Where(u => u.Id == userId).Select(u => u.UserName).FirstOrDefault(),
                Email = _dbContext.Set<IdentityUser>().Where(e => e.Id == userId).Select(e => e.Email).FirstOrDefault(),
                Phone = _dbContext.Set<IdentityUser>().Where(e => e.Id == userId).Select(e => e.PhoneNumber).FirstOrDefault(),
                Role = _dbContext.Set<IdentityRole>().Where(r => r.Id == roleId).Select(r => r.Name).FirstOrDefault(),
                AvatarName = _dbContext.Set<MyUserEntity>().Where(o => o.UserID == userId).Select(a => a.Avatar).FirstOrDefault(),
                IsOnline = _dbContext.Set<MyUserEntity>().Where(o => o.UserID == userId).Select(a => a.Active).FirstOrDefault()
            };
            return user;
        }

        public UserToViewInfo GetUserById(string userId)
        {
            var roleId = _dbContext.Set<IdentityUserRole<string>>().Where(o => o.UserId == userId).Select(o => o.RoleId).FirstOrDefault();
            UserToViewInfo user = new UserToViewInfo()
            {
                UserId = userId,
                UserName = _dbContext.Set<IdentityUser>().Where(u => u.Id == userId).Select(u => u.UserName).FirstOrDefault(),
                Email = _dbContext.Set<IdentityUser>().Where(e => e.Id == userId).Select(e => e.Email).FirstOrDefault(),
                Phone = _dbContext.Set<IdentityUser>().Where(e => e.Id == userId).Select(e => e.PhoneNumber).FirstOrDefault(),
                Role = _dbContext.Set<IdentityRole>().Where(r => r.Id == roleId).Select(r => r.Name).FirstOrDefault(),
                AvatarName = _dbContext.Set<MyUserEntity>().Where(o => o.UserID == userId).Select(a => a.Avatar).FirstOrDefault(),
                IsOnline = _dbContext.Set<MyUserEntity>().Where(o => o.UserID == userId).Select(a => a.Active).FirstOrDefault()
            };
            return user;
        }

        public List<UserToViewInfo> GetAllUsers()
        {
            return _dbContext.Set<IdentityUserRole<string>>().Select(o => new UserToViewInfo()
            {
                UserId = _dbContext.Set<IdentityUser>().Where(u => o.UserId == u.Id).Select(u => u.Id).FirstOrDefault(),
                UserName = _dbContext.Set<IdentityUser>().Where(u => o.UserId == u.Id).Select(u => u.UserName).FirstOrDefault(),
                Email = _dbContext.Set<IdentityUser>().Where(e => o.UserId == e.Id).Select(e => e.Email).FirstOrDefault(),
                Phone = _dbContext.Set<IdentityUser>().Where(e => o.UserId == e.Id).Select(e => e.PhoneNumber).FirstOrDefault(),
                Role = _dbContext.Set<IdentityRole>().Where(r => o.RoleId == r.Id).Select(r => r.Name).FirstOrDefault(),
                AvatarName = _dbContext.Set<MyUserEntity>().Where(u => o.UserId == u.UserID).Select(u => u.Avatar).FirstOrDefault(),
                IsOnline = _dbContext.Set<MyUserEntity>().Where(u => o.UserId == u.UserID).Select(u => u.Active).FirstOrDefault()
            }).ToList();
        }

        public async Task<IdentityResult> AddPhone(string userId, string phone)
        {
            var user = _dbContext.Set<IdentityUser>().Where(o => (o.Id == userId)).ToList().FirstOrDefault();
            //_userMan.SetPhoneNumberAsync(user, phone);
            return await _userMan.SetPhoneNumberAsync(user, phone);
            
        }

        //-----------------------------------------------------------------------------//
        // Realationships Code Block
        public List<UserToFriendModel> GetAllRealationships(string userId)
        {
            
            return _dbContext.friendList.Include(o => o.User).Include(o => o.Friend).Where(u => u.UserID == userId || u.FriendID == userId).Select(o => new UserToFriendModel()
            {
                UserId = o.User.Id,
                UserName = o.User.UserName,
                FriendId = o.Friend.Id,
                FriendName = o.Friend.UserName,
                Accepted = o.Accepted,
                Blocked = o.Blocked
            }).ToList();
        }

        public bool HasFriendRelation(string fromUserId, string toUserId)
        {
            return _dbContext.friendList.Where(flItem => (flItem.UserID == fromUserId && flItem.FriendID == toUserId) || (flItem.FriendID == fromUserId && flItem.UserID == toUserId)).Count() > 0 ? true : false;
        }

        public int SendFriendRequest(string fromUserId, string toUserId)
        {
            _dbContext.friendList.Add(new FriendListEntity() { UserID = fromUserId, FriendID = toUserId });
            _dbContext.SaveChanges();
            return 0;
        }

        public int SendBlockingRequest(string fromUserId, string toUserId)
        {
            _dbContext.friendList.Add(new FriendListEntity() { UserID = fromUserId, FriendID = toUserId, Accepted = false, Blocked = true });
            _dbContext.SaveChanges();
            return 0;
        }

        public int ProcessFriendRequest(string fromUserId, string toUserId, bool accepted)
        {
            var relationList = _dbContext.friendList.Where(flItem => (flItem.UserID == fromUserId && flItem.FriendID == toUserId) || (flItem.FriendID == fromUserId && flItem.UserID == toUserId)).ToList();
            if (relationList.Count == 1)
            {
                var relation = relationList.FirstOrDefault();
                if (!relation.Accepted)
                {
                    if (accepted)
                    {
                        relation.Accepted = true;
                        _dbContext.SaveChanges();
                        return 0;
                    }
                    else
                    {
                        return RemoveFriend(fromUserId, toUserId);
                    }
                }
                return -2;
            }

            return -1;
        }

        public int RemoveFriend(string fromUserId, string toUserId)
        {
            var relationList = _dbContext.friendList.Where(flItem => (flItem.UserID == fromUserId && flItem.FriendID == toUserId) || (flItem.FriendID == fromUserId && flItem.UserID == toUserId)).ToList();
            if (relationList.Count == 1)
            {
                var relation = relationList.FirstOrDefault();
                _dbContext.friendList.Remove(relation);
                _dbContext.SaveChanges();
                return 0;
            }

            return -1;
        }




        //----------------------------- Messages --------------------------------------//

        public async Task<List<MessagesByUsers>> GetOldMessages(string fromUserId, string toUserId) 
        {
             return _dbContext.messagesList.Where(o => (o.UserID == fromUserId && o.FriendID == toUserId) || (o.FriendID == fromUserId && o.UserID == toUserId)).Select(o => new MessagesByUsers()
            {
                UserID = o.User.UserName,
                FriendID = o.Friend.UserName,
                Content = o.Content,
                DateSent = o.DateSent
            }).ToList();
        }


        public async Task SaveMessages(string fromUserId, string toUserId, string content)
        {
            _dbContext.messagesList.Add(new MessagesByUsers()
            {
                UserID = fromUserId,
                FriendID = toUserId,
                Content = content,
                DateSent = DateTime.Now
            }) ;
            _dbContext.SaveChanges();
        }

        public async Task RemoveMessage(string fromUserId, string toUserId) 
        {
            var sum = _dbContext.messagesList.Where(o => (o.UserID == fromUserId && o.FriendID == toUserId) || (o.FriendID == fromUserId && o.UserID == toUserId)).Select(o => new MessagesByUsers()
            {
                UserID = o.User.Id,
                FriendID = o.Friend.Id,
                Content = o.Content,
                DateSent = o.DateSent
            }).ToList().Count();
             
                while (sum >= 20)
                {
                    var a = _dbContext.messagesList.Where(o => (o.UserID == fromUserId && o.FriendID == toUserId) || (o.FriendID == fromUserId && o.UserID == toUserId)).First();
                    _dbContext.messagesList.Remove(a);
                    _dbContext.SaveChanges();
                    sum --;
                }           
        } 

        //-----------------------------------------------------------------------------//
        public int AddUserInfo(string userId, bool isOnline)
        {
            var userinf = _dbContext.userInfoList.Where(o => (o.UserID == userId)).ToList();
            if (userinf.Count() == 0)
            {
                _dbContext.userInfoList.Add(new MyUserEntity() {
                    UserID = userId,
                    Active = isOnline,
                });
            }
            else
            {
                var user = userinf.FirstOrDefault();
                user.Active = isOnline;
            }
            _dbContext.SaveChanges();
            return 0;
        }

        public int AddUserAvatar(string userId, string avatar)
        {
            var user = _dbContext.userInfoList.Where(o => (o.UserID == userId)).ToList().FirstOrDefault();
            user.Avatar = avatar;
            _dbContext.SaveChanges();
            return 0;
        }



        //------------------------------------ POST ------------------------------------//

        public async Task<List<Post>> GetPost()
        {
            return _dbContext.postList.Include(o => o.User).Select(o => new Post()
            {
                Id = o.Id,
                UserID = o.User.Id,
                UserName = o.User.UserName,
                Content = o.Content,
                InsertionDate = o.InsertionDate,
                Like = o.Like,
            }).ToList();
        }

        public async Task SavePost(string userName, string message)
        {
            
            var userId = _dbContext.Set<IdentityUser>().Where(u => u.UserName == userName).Select(o => o.Id).FirstOrDefault();

            _dbContext.postList.Add(new Post()
            {
                UserID = userId,
                UserName = userName,
                Content = message,
                InsertionDate = DateTime.Now,
                Like = 0
            });
            _dbContext.SaveChanges();            
        }

        public async Task AddLike(string userName, int postId)
        {
            var userId = _dbContext.Set<IdentityUser>().Where(u => u.UserName == userName).Select(o => o.Id).FirstOrDefault();

            _dbContext.likeList.Add(new LikeForPost()
            {
                PostID = postId,
                UserID = userId
            });
            _dbContext.SaveChanges();
            _dbContext.postList.Where(o => o.Id == postId).FirstOrDefault().Like++;
            _dbContext.SaveChanges();
        }


        public async Task RemoveLike(string userName, int postId)
        {
            var userId = _dbContext.Set<IdentityUser>().Where(u => u.UserName == userName).Select(o => o.Id).FirstOrDefault();

            var post = _dbContext.likeList.Where(o => o.PostID == postId).FirstOrDefault();
            _dbContext.likeList.Remove(post);
            _dbContext.SaveChanges();

            _dbContext.postList.Where(o => o.Id == postId).FirstOrDefault().Like--;
            _dbContext.SaveChanges();
        }

        public async Task<List<LikeForPost>> GetLikeUsers(int postId) 
        {
            return _dbContext.likeList.Where(o => o.PostID == postId).ToList();       
        }


        //----------------------------------------------------------------------------//

        public void RemoveUserRole(string userId, string newRole)
        {
            var user = _userMan.FindByIdAsync(userId).Result;
            if (newRole == "ADMIN")
            {

                _userMan.RemoveFromRoleAsync(user, "USER").Wait();
            }
            else 
            {
                _userMan.RemoveFromRoleAsync(user, "ADMIN").Wait();
            }

            // _userMan.AddToRoleAsync(user, newRole);

        }
        public void ChangeUserRole(string userId, string newRole)
        {
            var user = _userMan.FindByIdAsync(userId).Result;

            _userMan.AddToRoleAsync(user, newRole);
            
        }

      /*
        public async Task DeleteUserAsync(string userId) 
        {
            var userInfo = _dbContext.userInfoList.Where(o => o.UserID == userId).FirstOrDefault();    
            _dbContext.userInfoList.Remove(userInfo);
            _dbContext.SaveChanges();

            var relationList = _dbContext.friendList.Where(flItem => (flItem.UserID == userId || flItem.FriendID == userId)).ToList();
            if (relationList.Count == 1)
            {
                var relation = relationList.FirstOrDefault();
                _dbContext.friendList.Remove(relation);
                _dbContext.SaveChanges();      
            }
            var userPosts = _dbContext.postList.Where(o => o.UserID == userId).ToList();
            foreach (var post in userPosts) 
            {
                _dbContext.postList.Remove(post);
                _dbContext.SaveChanges();
            }


            var user = _userMan.FindByIdAsync(userId).Result;
            var userRole = _userMan.GetRolesAsync(user);
            _dbContext.Users.Remove(user);
            _dbContext.SaveChanges();
        }
      */
    }
}
