﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using SN_WebApp.Repositories;
namespace SN_WebApp
{
    public class InitDbRoles
    {
        public static void Seed(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            
            /*
            var roleAdmin = new IdentityRole("ADMIN");
            roleAdmin.Id = "1";
            var roleUser = new IdentityRole("USER");
            roleUser.Id = "2";

            roleManager.CreateAsync(roleAdmin).Wait();
            roleManager.CreateAsync(roleUser).Wait();
            */
            roleManager.CreateAsync(new IdentityRole("ADMIN")).Wait();
            roleManager.CreateAsync(new IdentityRole("USER")).Wait();

            var adminUser = new IdentityUser("admin@admin.cz");
            adminUser.Email = "admin@admin.cz";
            adminUser.UserName = "Administrator";
            adminUser.EmailConfirmed = true;
            userManager.CreateAsync(adminUser).Wait();
            userManager.AddPasswordAsync(adminUser, "Admin1_").Wait();
            userManager.AddToRoleAsync(adminUser, "ADMIN").Wait();
        
        }

    }
}
