﻿using SN_WebApp.Repositories;
using SN_WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN_WebApp.Services
{
    public class PostService : IPostService
    {

        private IUserRepository _userRepo;

        public PostService(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public async Task<List<MessageModel>> GetPostAsync(int views)
        {
            List<MessageModel> list = new List<MessageModel>();
            List<MessageModel> viewList = new List<MessageModel>();
            var posts = await _userRepo.GetPost();
            foreach (var post in posts)
            {
                list.Add(new MessageModel
                {
                    Id = post.Id,
                    Message = post.Content,
                    FromUser = post.UserName,
                    DateSent = post.InsertionDate,
                    Like = post.Like,
                });

            }
            list.Sort((x, y) => DateTime.Compare(y.DateSent, x.DateSent));
            if (list.Count() > views)
            {
                viewList = list.GetRange(0, views);
                //viewList.Sort((x, y) => DateTime.Compare(y.DateSent, x.DateSent));
                return viewList;
            }
            else
            {
                //list.Sort((x, y) => DateTime.Compare(y.DateSent, x.DateSent));
                return list;
            }
        }

        public async Task<List<MessageModel>> GetPostByNameAsync(string userName, int views)
        {
            List<MessageModel> list = new List<MessageModel>();
            List<MessageModel> viewList = new List<MessageModel>();
            var posts = await _userRepo.GetPost();
            foreach (var post in posts)
            {
                if (post.UserName == userName) 
                {
                    list.Add(new MessageModel
                    {
                        Id = post.Id,
                        Message = post.Content,
                        FromUser = post.UserName,
                        DateSent = post.InsertionDate,
                        Like = post.Like,
                    });
                }
            }
            list.Sort((x, y) => DateTime.Compare(y.DateSent, x.DateSent));
            if (list.Count() > views)
            {
                viewList = list.GetRange(0, views);
                //viewList.Sort((x, y) => DateTime.Compare(y.DateSent, x.DateSent));
                return viewList;
            }
            else
            {
                //list.Sort((x, y) => DateTime.Compare(y.DateSent, x.DateSent));
                return list;
            }
        }

        public async Task SavePostAsync(string userName, string message)
        {
            _userRepo.SavePost(userName, message);
        }

        public async Task<List<UserToViewInfo>> GetLikeUsers(int postId)
        {
            List<UserToViewInfo> usersInfo = new List<UserToViewInfo>();
            var postInf = await _userRepo.GetLikeUsers(postId);
            foreach (var inf in postInf)
            {
                usersInfo.Add(_userRepo.GetUserById(inf.UserID));
            }
            return usersInfo;
        }
        public async Task AddLike(string userName, int postId)
        {
            List<UserToViewInfo> usersInfo = await GetLikeUsers(postId);
            if (usersInfo.Exists(o => o.UserName == userName))
            {
                _userRepo.RemoveLike(userName, postId);
            }
            else
            {
                _userRepo.AddLike(userName, postId);
            }

        }

    }
}
