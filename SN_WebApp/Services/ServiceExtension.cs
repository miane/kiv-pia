﻿
using SN_WebApp.Services;
using SN_WebApp.Repositories;
using Microsoft.Extensions.DependencyInjection;



namespace SN_WebApp.Services
{
    public static class ServiceExtension
    {

        public static void AddServices(this IServiceCollection servColl)
        {
            servColl.AddTransient<IUserService, UserService>();
            servColl.AddTransient<IFriendService, FriendService>();
            servColl.AddTransient<IPostService, PostService>();
            servColl.AddTransient<IUserRepository, UserRepository>();
        }
    }
}
