using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading.Tasks;
using System.Security.Claims;
using SN_WebApp.Models;

namespace SN_WebApp.Services
{

    [Route("api")]
    [ApiController]
    [Produces("application/json")]
    public class Controller : ControllerBase
    {
        private IUserService _userService;
        public Controller(IUserService userService)
        {
            _userService = userService;
        }




        [HttpPost("AddPhoneNumber")]
        public async Task<IActionResult> AddPhoneNumber([FromBody] UserToViewInfo model)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            //string userId = _userService.GetUserIdByName(userId);
            if (userId is null)
            {
                return NotFound();
            }

            var result = await _userService.AddPhone(userId, model.Phone);
            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    
    }
}