﻿using SN_WebApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SN_WebApp.Services
{
    public interface IFriendService
    {
        public List<UserToFriendModel> GetAllFriends(string userId);
        public List<UserToViewInfo> GetAllFriendsByName(string userName);
        public int SendFriendRequest(string fromUserId, string toUserId);
        public int SendBlockingRequest(string fromUserId, string toUserId);
        public int ProcessFriendrequest(string fromUserId, string toUserId, bool accepted);
        public int RemoveFriend(string meUserId, string friendUserId);
        public bool FriendRequest(string userName);
        List<UserToFriendModel> GetAllRelationships(string userName);
        Task RemoveMessage(string fromUserId, string toUserId);
        Task SaveMessages(string fromUserName, string toUserName, string content);
        Task<List<MessageModel>> GetOldMessages(string fromUserName, string toUserName);


    }
}
