﻿using SN_WebApp.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SN_WebApp.Services
{
    public interface IUserService
    {

        public UserToViewInfo GetUserByName(string userName);
        public UserToViewInfo GetUserNameById(string userId);
        
        Task<IdentityResult> AddPhone(string userId, string phone);
        public List<UserToViewInfo> GetAllUsers();
       // Task DeleteUser(string userId);
        public bool ChangeUserRole(string userId, string newRole);

        public int AddUser(string userId, bool isOnline);
        public int AddAvatar(string userName, string avatar);
    }
}
