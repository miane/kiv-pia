﻿using SN_WebApp.Models;
using SN_WebApp.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SN_WebApp.Services
{
    public class FriendService : IFriendService
    {
        private IUserRepository _userRepo;

        public FriendService(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public List<UserToFriendModel> GetAllFriends(string userId)
        {
            return _userRepo.GetAllRealationships(userId);
        }

        public List<UserToFriendModel> GetAllRelationships(string userName)
        {
            string userId = _userRepo.GetIdByName(userName);
            return _userRepo.GetAllRealationships(userId);
        }

        public List<UserToViewInfo> GetAllFriendsByName(string userName)
        {
            List<UserToViewInfo> listfriends = new List<UserToViewInfo>();
            string userId = _userRepo.GetIdByName(userName);
            var relation = _userRepo.GetAllRealationships(userId);

            foreach (var friend in relation)
            {
                if (!friend.Blocked && friend.Accepted)
                {
                    if (userName != friend.FriendName)
                    {
                        listfriends.Add(_userRepo.GetUserByName(friend.FriendName));
                    }
                    else 
                    {
                        listfriends.Add(_userRepo.GetUserByName(friend.UserName));
                    }
                }
            }
            return listfriends;
        }

        public bool FriendRequest(string userName) 
        {
            string userId = _userRepo.GetIdByName(userName);
            var relation = _userRepo.GetAllRealationships(userId);
            foreach (var friend in relation)
            {
                if (!friend.Blocked && !friend.Accepted)
                {
                    if (userId != friend.FriendName)
                    {
                        return true;
                        }
                }
            }
            return false;
        }

        public int ProcessFriendrequest(string fromUserId, string toUserId, bool accepted)
        {
            return _userRepo.ProcessFriendRequest(fromUserId, toUserId, accepted);
        }

        public int RemoveFriend(string meUserId, string friendUserId)
        {
            return _userRepo.RemoveFriend(meUserId, friendUserId);
        }

        public int SendBlockingRequest(string fromUserId, string toUserId)
        {
            if (_userRepo.HasFriendRelation(fromUserId, toUserId))
            {
                _userRepo.RemoveFriend(fromUserId, toUserId);
                return _userRepo.SendBlockingRequest(fromUserId, toUserId);
            }
            else
            {
                return _userRepo.SendBlockingRequest(fromUserId, toUserId);
            }
        }

        public int SendFriendRequest(string fromUserId, string toUserId)
        {
            if (_userRepo.HasFriendRelation(fromUserId, toUserId))
            {
                return -1;
            }
            else
            {
                return _userRepo.SendFriendRequest(fromUserId, toUserId);
            }
        }

        //---------------------------------- Messages ------------------------------------//
        public async Task RemoveMessage(string fromUserId, string toUserId) 
        {
            await _userRepo.RemoveMessage(fromUserId, toUserId);
        }

        public async Task SaveMessages(string fromUserName, string toUserName, string content) 
        {
            var userId = _userRepo.GetIdByName(fromUserName);
            var friendId = _userRepo.GetIdByName(toUserName);

            await _userRepo.SaveMessages(userId, friendId, content);
        }
        public async Task<List<MessageModel>> GetOldMessages(string fromUserName, string toUserName) 
        {
            List<MessageModel> list = new List<MessageModel>();
            var userId = _userRepo.GetIdByName(fromUserName);
            var friendId = _userRepo.GetIdByName(toUserName);

            var messages = await _userRepo.GetOldMessages(userId, friendId);
            if (messages.Count() > 20)
            {
               await _userRepo.RemoveMessage(userId, friendId);
            }
           
            foreach (var mess in messages)
            {
                list.Add(new MessageModel()
                {
                    FromUser = mess.UserID,
                    ToUser = mess.FriendID,
                    Message = mess.Content,
                    DateSent = mess.DateSent,
                    CurrentUser = mess.UserID == fromUserName ? true : false
                });

            }
            return list;
        }
    }
}
