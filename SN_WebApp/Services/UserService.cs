﻿using SN_WebApp.Models;
using SN_WebApp.Repositories;
using SN_WebApp.Services;
using SN_WebApp.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SN_WebApp.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepo;

        public UserService(IUserRepository userRepo)
        {
            _userRepo = userRepo;
        }

        public async Task<IdentityResult> AddPhone(string userId, string phone) 
        {  
          return await _userRepo.AddPhone(userId, phone);
        }


        public UserToViewInfo GetUserByName(string userName)
        {
            return _userRepo.GetUserByName(userName);
        }

        public UserToViewInfo GetUserNameById(string userId)
        {
            return _userRepo.GetUserById(userId);
        }

        public List<UserToViewInfo> GetAllUsers()
        {
            return _userRepo.GetAllUsers();
        }

       /* public async Task DeleteUser(string userId)
        {
            await _userRepo.DeleteUserAsync(userId);
        }*/


        public bool ChangeUserRole(string userId, string newRole)
        {
            if (newRole == "ADMIN" || newRole == "USER")
            {
                _userRepo.RemoveUserRole(userId, newRole);
                _userRepo.ChangeUserRole(userId, newRole);
                return true;
            }
            return false;
        }

        public int AddUser(string userId, bool isOnline) 
        {
            return _userRepo.AddUserInfo(userId, isOnline);
        }

        public int AddAvatar(string userName, string avatar)
        {
            string userId = _userRepo.GetIdByName(userName);
            return _userRepo.AddUserAvatar(userId, avatar);
        }

     
    }
}
