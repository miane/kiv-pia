﻿using SN_WebApp.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SN_WebApp.Services
{
    public interface IPostService
    {
        Task AddLike(string userName, int postId);
        Task<List<UserToViewInfo>> GetLikeUsers(int postId);
        Task<List<MessageModel>> GetPostAsync(int views);
        Task SavePostAsync(string userName, string message);

        Task<List<MessageModel>> GetPostByNameAsync(string userName, int views);
    }
}