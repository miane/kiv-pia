# Social Network

This is a Social Network, made as a semestral project for KIV/PIA. 

## Run

docker-compose up --build (sometimes is necessary again docker-compose up)

At startup is created an admin account 
- admin@admin.cz 
- Admin1_ 

## Architecture

### Overview

- Blazor Server-side
- ASP.NET Core 5.0
    - Entity Framework
- Microsoft SQL
- Swagger
- HTML + CSS
    - [Bootstrap](https://getbootstrap.com/)
- REST
    - GET / POST
- Web sockets
    - [SignalR](https://dotnet.microsoft.com/apps/aspnet/signalr)
- Docker
- Docker compose

## Database

Databáze je typu MySQL a má následující tabulky pro korektní práci se hrou.

### Table for post

- ID
- User ID
- User Name
- Content
- Insertion Date
- Likes for post

### Table for post history 

- ID
- Post ID
- User ID

### Table with additional info about users

Complements the used table from the Entity Framework
- User ID
- User Name
- Profile photo
- (bool) is user online

### Table for relationship

The link between friends is reciprocal, if user A is friend B, then B is also friend A. No further record is needed. 
- ID
- User ID
- User Name
- Friend ID
- Friend Name 
- Accepted
- Blocked

### Table for messages

- ID
- User ID
- Friend ID
- Content
- Date sent 

## Applications

The application could be divided into three main units.

The first part is the Database, Entities in the Data folder, UserRepository, and NetworkDBContext. The database structure is defined by the NetworkDBContext class. Entities are in the database and the communication with the database is mediated by the UserRepository.

There are FriendService and UserService for working with logic. These mediate services for managing the application or users and internally call the Repository functions defined in the IUserRepository interfaces. Data models that need to be passed to the user part of the application could also be included here.

There is a Hub for Web Socket communication, which handles calls / listens on sockets and manages responses. 

The user part then contains pages defined by the Entity framework (Areas / Identity), which are used to register, log in, manage user information, etc. It also contains * .razor files in the Pages and Shares folders. These files can be taken as components.
The component class is usually written in the form of a Razor markup page with a .razor file extension. Components in Blazor are formally referred to as Razor components. Razor is a syntax for combining HTML markup with C # code designed for developer productivity. 

### Implemented properties 

## Friendlist 

- Realitme search by user name (at least three characters must be provided)
    - exclude blocked user and friends
    - include users with pending friend requests
    - friendship request or blocked
- list of friends/pending friend requests
    - accepting friendship
    - rejecting friendship
    - blocking friendship 
- list of blocked users
    - unblocking a user

## Postlist 

Post list is implemented like component to homepage.

## Chatrooms

Only available if the user has one or more friends.
- Popup chat
- Chatroom


## UserProfile 

- info about user
- post from current user
- friend from current user

- when it is a user profile
    - modify profile picture
    - create new post

## Notification

At the bottom right ;D
- for new message (and popup chat) - green circle around the envelope 
- for new friendship - green circle around the robot
- admin notification - pop up window


## Bonus part

- Likes
- Possibility to show older posts
- Storing of chat messages in the DB (last 20 between two users)
